#include "gtest/gtest.h"

#include "hasherLib/programOptions.h"
#include "hasherLib/programParameter.h"
#include "hasherLib/fileHasher.h"
#include <hasherLib/hashFunctions.h>

#include <cstdint>
#include <string>
#include <sstream>

TEST(programOptions, testIncorrectNumberOfInput) {
	const char* arr[] = {"1", "2"};

	ProgramParameter parameter;

	EXPECT_EQ(false, ProgramOptions::getProgramParam(arr, 0, parameter));
	EXPECT_EQ(false, ProgramOptions::getProgramParam(arr, 2, parameter));
	EXPECT_EQ(false, ProgramOptions::getProgramParam(arr, 4, parameter));
}

TEST(programOptions, testCorrectInput) {
	const char* arr1[] = {"-m", "sum32", "t.txt"};
	const char* arr2[] = {"t.txt", "-m", "crc32"};
	const char* arr3[] = {"1", "2", "3"};
	const char* arr4[] = {"-m", "incorrectParam", "t.txt"};

	ProgramParameter parameter;

	EXPECT_EQ(true, ProgramOptions::getProgramParam(arr1, 3, parameter));
	EXPECT_EQ(HashMode::SUM32, parameter.mode);
	EXPECT_EQ("t.txt", parameter.fileName);

	EXPECT_EQ(true, ProgramOptions::getProgramParam(arr2, 3, parameter));
	EXPECT_EQ(HashMode::CRC32, parameter.mode);
	EXPECT_EQ("t.txt", parameter.fileName);

	EXPECT_EQ(false, ProgramOptions::getProgramParam(arr3, 3, parameter));

	EXPECT_EQ(false, ProgramOptions::getProgramParam(arr4, 3, parameter));
}

TEST(fileHasher, testIncorrectFile) {
	const char* arr1[] = {"-m", "sum32", "incorrectName.txt"};
	ProgramParameter parameter;
	ProgramOptions::getProgramParam(arr1, 3, parameter);

	FileHasher hasher(parameter);
	uint32_t result = 0;

	EXPECT_EQ(false, hasher.generateHashCode(result));
}

TEST(fileHasher, testCorrectFile) {
	const char* arr1[] = {"-m", "sum32", "t.txt"};
	ProgramParameter parameter;
	ProgramOptions::getProgramParam(arr1, 3, parameter);

	FileHasher hasher(parameter);
	uint32_t result = 0;
	
	EXPECT_EQ(true, hasher.generateHashCode(result));
}

TEST(HashFunctions, testZeroFile) {
	uint32_t result = 0;
	std::string str1("");
	std::string str2("");
	std::istringstream stream1(str1);
	std::istringstream stream2(str2);
	EXPECT_EQ(0, HashFunctions::generateHashForSUM32(stream1));
	EXPECT_EQ(0, HashFunctions::generateHashForCRC32(stream2));
}

TEST(HashFunctions, testSingleFile) {
	uint32_t result = 0;
	std::string str1("a");
	std::string str2("a");
	std::istringstream stream1(str1);
	std::istringstream stream2(str2);
	EXPECT_EQ(97, HashFunctions::generateHashForSUM32(stream1));
	EXPECT_EQ(3904355907, HashFunctions::generateHashForCRC32(stream2));
}

TEST(HashFunctions, testMultipleOfFourFile) {
	uint32_t result = 0;
	std::string str1("aaaa");
	std::string str2("aaaa");
	std::istringstream stream1(str1);
	std::istringstream stream2(str2);
	EXPECT_EQ(1633771873, HashFunctions::generateHashForSUM32(stream1));
	EXPECT_EQ(2912478533, HashFunctions::generateHashForCRC32(stream2));
}

TEST(HashFunctions, testNotAMultipleOfFourFile) {
	uint32_t result = 0;
	std::string str1("aaaaa");
	std::string str2("aaaaa");
	std::istringstream stream1(str1);
	std::istringstream stream2(str2);
	EXPECT_EQ(1633771873 + 97, HashFunctions::generateHashForSUM32(stream1));
	EXPECT_EQ(4004287417, HashFunctions::generateHashForCRC32(stream2));
}

TEST(HashFunctions, testBigFile) {
	uint32_t result = 0;
	std::string str1("qwertyuiopasdfghjklzxcvbnm");
	std::string str2("qwertyuiopasdfghjklzxcvbnm");
	std::istringstream stream1(str1);
	std::istringstream stream2(str2);
	EXPECT_EQ(2627138815, HashFunctions::generateHashForSUM32(stream1));
	EXPECT_EQ(3228254425, HashFunctions::generateHashForCRC32(stream2));
}

TEST(HashFunctions, testLittleFile) {
	uint32_t result = 0;
	std::string str1("qwertyu");
	std::string str2("qwertyu");
	std::istringstream stream1(str1);
	std::istringstream stream2(str2);
	EXPECT_EQ(1911283431, HashFunctions::generateHashForSUM32(stream1));
	EXPECT_EQ(3705286194, HashFunctions::generateHashForCRC32(stream2));
}
