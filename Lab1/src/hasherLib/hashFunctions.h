#pragma once

#include <cstdint>
#include <iosfwd>

namespace HashFunctions {
	uint32_t generateHashForSUM32(std::istream& stream);
	uint32_t generateHashForCRC32(std::istream& stream);
}
