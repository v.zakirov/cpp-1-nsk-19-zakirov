#include "fileHasher.h"

#include "hashFunctions.h"

#include <fstream>
#include <iostream>
#include <cstdint>

namespace {
	constexpr const char* fileNotOpened = "A file with this name has not been opened, check the file name and path\n";
}

FileHasher::FileHasher(ProgramParameter& param) : _file(param.fileName, std::ios_base::binary), _mode(param.mode) {}

bool FileHasher::generateHashCode(uint32_t& result) {
	if (!_file) {
		std::cerr << fileNotOpened;
		return false;
	}

	switch (_mode) {
		case HashMode::SUM32 : {
			result = HashFunctions::generateHashForSUM32(_file);
			return true;
		}
		case HashMode::CRC32 : {
			result = HashFunctions::generateHashForCRC32(_file);
			return true;
		}
		default : {
			return false;
		}
	}
}
