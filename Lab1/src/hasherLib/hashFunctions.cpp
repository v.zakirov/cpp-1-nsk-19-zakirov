#include "hashFunctions.h"

#include <istream>
#include <cstdint>

constexpr const int byteSize = 8;

uint32_t HashFunctions::generateHashForSUM32(std::istream& stream) {
	uint32_t newElementOfSum(0);	
	uint32_t sum(0);
	int counter(0);

	uint8_t buf(0);
	while (stream.read((char*)&buf, 1)) {
		newElementOfSum <<= byteSize;
		newElementOfSum |= buf;
		++counter;
		if (4 == counter) {
			counter = 0;
			sum += newElementOfSum;
			newElementOfSum = 0;
		}
	}

	sum += newElementOfSum;
	return sum;
}

uint32_t HashFunctions::generateHashForCRC32(std::istream& stream) {
	uint32_t crcTab[256];
	uint32_t crc;
	for (int i = 0; i < 256; ++i) {
		crc = i;
		for (int j = 0; j < 8; ++j) {
			crc = crc & 1 ? (crc >> 1) ^ 0xEDB88320 : crc >> 1;
		}
		crcTab[i] = crc;
	}
	crc = 0xFFFFFFFF;
	uint8_t buf(0);
	while (stream.read((char*)&buf, 1)) {
		crc = crcTab[(crc ^ buf) & 0xFF] ^ (crc >> 8);
	}
	return crc ^ 0xFFFFFFFF	;
}
