#include "programOptions.h"

#include <cstring>
#include <iostream>
#include <cassert>
#include <utility>

namespace {
	constexpr const  char* helpIncorrectNumberOfArgumentsMessage = 
		"The input must look like\n\t"
		"./hasher -h //for help\n\t"
		"The key \"-m\" is required, followed by {sum32, crc32} to choose from\n\t"
		"The last argument is the file name\n";

	constexpr const  char* oneArgumentErrMessage = "If you passed one argument it should be the key \"-h\"\n";

	constexpr const  char* availableInputMessage = 
		"To enter you availabl\n\t"
		"./hasher <filename> -m <mode>\n\t"
		"./hasher -m <mode> <filename>\n\t"
		"./hasher -h\n";


	bool oneKey(const char** arg, ProgramParameter& param) {
		if (0 == strcmp("-h", arg[0])) {
			param.isHelp = true;
			param.message += helpIncorrectNumberOfArgumentsMessage;
			return true;
		} else {
			param.message += oneArgumentErrMessage;
			return false;
		}
	}

	int findModeKeyIndex(const char** arg, int number) {
		const int indexModeKey = -1;
		for (int i = 0; i < number; ++i) {
			if (0 == strcmp("-m", arg[i])) {
				return i;
			}
		}
		return indexModeKey;
	}

	void setFileName(const char** arg, int number, int indexModeKey, ProgramParameter& param) {
		if (0 == indexModeKey) {
			param.fileName = arg[2];
		}
		if ((number - 2) == indexModeKey) {
			param.fileName = arg[0];
		}
	}

	bool setMode(const char** arg, int number, int indexModeKey, ProgramParameter& param) {
		if (0 > indexModeKey || (number - 1) <= indexModeKey) {
			param.message += helpIncorrectNumberOfArgumentsMessage;
			param.message += availableInputMessage;
			return false;
		}

		if (0 == strcmp("sum32", arg[indexModeKey + 1])) {
			param.mode = HashMode::SUM32;
			return true;
		}
		if (0 == strcmp("crc32", arg[indexModeKey + 1])) {
			param.mode = HashMode::CRC32;
			return true;
		}
		param.message += "Invalid parameter of mode\n";
		param.message += helpIncorrectNumberOfArgumentsMessage;
		return false;
	}
} // namespace

bool ProgramOptions::getProgramParam(const char** arg, int number, ProgramParameter& param) {
	param.isHelp = false;
	if (1 != number && 3 != number) {
		param.message += "You have entered an incorrect number of arguments\n";
		param.message += helpIncorrectNumberOfArgumentsMessage;
		param.message += '\n';
		return false;
	}

	if (1 == number) {
		return oneKey(arg, param);
	}

	int indexModeKey = findModeKeyIndex(arg, number);
	if (!setMode(arg, number, indexModeKey, param)) {
		return false;
	}
	setFileName(arg, number, indexModeKey, param);

	return true;
}
