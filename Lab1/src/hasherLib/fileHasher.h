#pragma once

#include "programParameter.h"

#include <fstream>
#include <cstdint>

class FileHasher {
public:
	FileHasher(ProgramParameter& param);
	bool generateHashCode(uint32_t& result);

private:
	std::ifstream _file;
	HashMode _mode;
};
