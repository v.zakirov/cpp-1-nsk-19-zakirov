#pragma once

#include "programParameter.h"

namespace ProgramOptions {
	bool getProgramParam(const char** arg, int number, ProgramParameter& param);
}
