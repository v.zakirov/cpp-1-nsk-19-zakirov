#pragma once

#include <string>

enum class HashMode {
	CRC32,
	SUM32,
};

struct ProgramParameter {
	bool isHelp;
	std::string message;
	HashMode mode;
	std::string fileName;
};
