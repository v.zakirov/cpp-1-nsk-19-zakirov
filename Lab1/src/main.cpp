#include "hasherLib/programOptions.h"
#include "hasherLib/programParameter.h"
#include "hasherLib/fileHasher.h"

#include <cstdint>
#include <iostream>

int main(int argc, const char** argv) {
	ProgramParameter programParameter;
	if (!ProgramOptions::getProgramParam(argv + 1, argc - 1, programParameter)) {
		std::cerr << programParameter.message << std::endl;
		return -1;
	}
	if (programParameter.isHelp) {
		std::cout << programParameter.message << std::endl;
		return 0;
	}
	FileHasher hasher(programParameter);
	uint32_t result = 0;
	if (!hasher.generateHashCode(result)) {
		return -2;
	}
	std::cout << result << std::endl;
	return 0;
}
