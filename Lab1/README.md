Lab work number one. Hashing a file in two ways.

Clone this repository, go to the "build" directory.
Run the "cmake way" command (you must have cmake installed),
where "way" is the path to the Lab1 directory.

To use it, run "hasher" and pass arguments. 
Possible argument:
	./hasher -m <mode> <filename>
	./hasher <filename> -m <mode>
	./hasher -h
Last command for help.
Available mod:
	sum32
	crc32
To run tests
	./hasherTests