# Lab work number three


```

# clone git repo
git clone https://gitlab.com/v.zakirov/cpp-1-nsk-19-zakirov.git
cd Lab3

# create temp directories for building
mkdir build

# generate, build
cd build
cmake ../
make

# run
./exeInterpreter fileP
#where fileP is the path to the file 

# the result will be written in out.txt in build derictories
```
