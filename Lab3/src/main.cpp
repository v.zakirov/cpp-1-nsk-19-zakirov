#include <interpreter/interpreter.h>
#include <interpreter/parser/parser.h>
#include <interpreter/exception/interpreterException.h>

#include <iostream>

int main(int argc, char const *argv[]) {
	if (2 != argc) {
		std::cerr << "Incorrect number of arguments" << std::endl;
		return -1;
	}

	try {
		Interpreter interpreter;
		Parser::parseCommands(argv[1], interpreter);
	}
	catch(const MyException::InterptreterException& exc) {
		std::cerr << exc.what() << std::endl;
		return -1;
	}
	catch(const std::bad_alloc& exp) {
		std::cerr << exp.what() << std::endl;
		return -1;
	}

	return 0;
}