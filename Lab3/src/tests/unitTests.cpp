#include "gtest/gtest.h"
#include <gmock/gmock.h>

#include <interpreter/interpreterInterface.h>
#include <interpreter/figure/allFigures.h>
#include <interpreter/parser/parser.h>

#include <memory>
#include <fstream>

TEST(interptreter, circle) {
	int color(0);
	int x(0);
	int y(0);
	int radius(1);
	Circle figure(color, x, y, radius);

	EXPECT_EQ(true, figure.containsDot(0, 1));
	EXPECT_EQ(true, figure.containsDot(0, -1));
	EXPECT_EQ(true, figure.containsDot(1, 0));
	EXPECT_EQ(true, figure.containsDot(-1, 0));
	EXPECT_EQ(false, figure.containsDot(1, 1));
	EXPECT_EQ(false, figure.containsDot(1, -1));
	EXPECT_EQ(false, figure.containsDot(-1, 1));
	EXPECT_EQ(false, figure.containsDot(-1, -1));

	figure.scale(2);

	EXPECT_EQ(true, figure.containsDot(0, 2));
	EXPECT_EQ(true, figure.containsDot(0, -2));
	EXPECT_EQ(true, figure.containsDot(2, 0));
	EXPECT_EQ(true, figure.containsDot(-2, 0));
	EXPECT_EQ(false, figure.containsDot(2, 2));
	EXPECT_EQ(false, figure.containsDot(2, -2));
	EXPECT_EQ(false, figure.containsDot(-2, 2));
	EXPECT_EQ(false, figure.containsDot(-2, -2));

	EXPECT_EQ(color, figure.getColor());

	figure.setColor(2);

	EXPECT_EQ(2, figure.getColor());
}

TEST(interptreter, square) {
	int color(0);
	int x(-1);
	int y(-1);
	int h(2);
	Square figure(color, x, y, h);

	EXPECT_EQ(true, figure.containsDot(1, 1));
	EXPECT_EQ(true, figure.containsDot(1, -1));
	EXPECT_EQ(true, figure.containsDot(-1, 1));
	EXPECT_EQ(true, figure.containsDot(-1, -1));

	figure.scale(2);

	EXPECT_EQ(true, figure.containsDot(2, 2));
	EXPECT_EQ(true, figure.containsDot(2, -2));
	EXPECT_EQ(true, figure.containsDot(-2, 2));
	EXPECT_EQ(true, figure.containsDot(-2, -2));

	EXPECT_EQ(color, figure.getColor());

	figure.setColor(2);

	EXPECT_EQ(2, figure.getColor());
}

TEST(interptreter, rectangle) {
	int color(0);
	int x(-1);
	int y(-2);
	int hx(2);
	int hy(4);
	Rectangle figure(color, x, y, hx, hy);

	EXPECT_EQ(true, figure.containsDot(1, 2));
	EXPECT_EQ(true, figure.containsDot(1, -2));
	EXPECT_EQ(true, figure.containsDot(-1, 2));
	EXPECT_EQ(true, figure.containsDot(-1, -2));

	figure.scale(2);

	EXPECT_EQ(true, figure.containsDot(2, 4));
	EXPECT_EQ(true, figure.containsDot(2, -4));
	EXPECT_EQ(true, figure.containsDot(-2, 4));
	EXPECT_EQ(true, figure.containsDot(-2, -4));

	EXPECT_EQ(color, figure.getColor());

	figure.setColor(2);

	EXPECT_EQ(2, figure.getColor());
}

TEST(interptreter, triangle) {
	int color(0);
	int x(0);
	int y(0);
	int hx(2);
	int hy(6);
	Triangle figure(color, x, y, hx, hy);

	EXPECT_EQ(true, figure.containsDot(0, 0));
	EXPECT_EQ(true, figure.containsDot(0, 4));
	EXPECT_EQ(true, figure.containsDot(2, 0));

	figure.scale(2);

	EXPECT_EQ(true, figure.containsDot(-2 / 3, -4 /3));
	EXPECT_EQ(true, figure.containsDot(0, -2));
	EXPECT_EQ(true, figure.containsDot(2, 0));

	EXPECT_EQ(color, figure.getColor());

	figure.setColor(2);

	EXPECT_EQ(2, figure.getColor());
}

class InterpreterMock : public InterpreterInterface {
public:
	MOCK_METHOD(void, addNewFigure, (const int id, std::unique_ptr<Figure>&& figure), (override));
	MOCK_METHOD(void, setColorById, (const int id, const int color), (override));
	MOCK_METHOD(void, moveObject, (const int id, const double dx, const double dy), (override));
	MOCK_METHOD(void, scaleObject, (const int id, const double scale), (override));
	MOCK_METHOD(void, bringToBackObject, (const int id), (override));
	MOCK_METHOD(void, bringToFrontObject, (const int id), (override));
	MOCK_METHOD(void, drawPicture, (const std::string& fileName
										, const double sx
										, const double sy
										, const double sw
										, const double sh
										, const int wp
										, const int hp)
									, (override));
};


TEST(interpreter, parser) {
	using ::testing::AtLeast;

	{
		std::string str = "create triangle 0 0 0 0 2 4\n"
							"create triangle 1 1 0 0 2 4\n"
							"create triangle 2 2 0 0 2 4\n"
							"create triangle 3 3 0 0 2 4\n"
							"set color 0 0\n"
							"move 1 2 -0.4\n"
							"scale 2 0.2\n"
							"bring to back 3\n"
							"bring to front 2\n"
							"draw out.txt 0 0 4 4 30 30";
		std::ofstream fileCommands("test.txt");
		fileCommands << str;
	}
	
	InterpreterMock inter;
	EXPECT_CALL(inter, setColorById(0, 0))
		.Times(AtLeast(1));

	EXPECT_CALL(inter, moveObject(1, 2, -0.4))
		.Times(AtLeast(1));

	EXPECT_CALL(inter, scaleObject(2, 0.2))
		.Times(AtLeast(1));	

	EXPECT_CALL(inter, bringToFrontObject(2))
		.Times(AtLeast(1));	

	EXPECT_CALL(inter, bringToBackObject(3))
		.Times(AtLeast(1));	

	EXPECT_CALL(inter, drawPicture("out.txt", 0, 0, 4, 4, 30, 30))
		.Times(AtLeast(1));	


	Parser::parseCommands("test.txt", inter);
}
