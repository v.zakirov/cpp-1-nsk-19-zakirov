#include "parser.h"
#include <interpreter/figure/allFigures.h>
#include <interpreter/exception/interpreterException.h>

#include <fstream>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

namespace {
	
	using funRefHandler = void (*)(InterpreterInterface&, std::vector<std::string>&);
	using figureBuilderFunRef = std::unique_ptr<Figure> (*)(std::vector<std::string>&);

	std::unique_ptr<Figure> getCircle(std::vector<std::string>& commands) {
		if (4 != commands.size()) {
			throw MyException::IncorrectNumberOfArgumentsException();
		}

		try {
			int color(std::stoi(commands[0]));
			double x(std::stod(commands[1]));
			double y(std::stod(commands[2]));
			double radius(std::stod(commands[3]));

			if (0 >= radius) {
				throw MyException::IncorrecValuesOAgrumentsException(std::to_string(radius));
			}
			if (0 > color || size_t(color) >= colors.size()) {
				throw MyException::IncorrecValuesOAgrumentsException("color " + std::to_string(color));
			}

			return std::make_unique<Circle>(color, x, y, radius);
		}
		catch(const std::invalid_argument&) {
			throw MyException::InvalidCastException();
		}
	}

	std::unique_ptr<Figure> getRectangle(std::vector<std::string>& commands) {
		if (5 != commands.size()) {
			throw MyException::IncorrectNumberOfArgumentsException();
		}

		try {
			int color(std::stoi(commands[0]));
			double x(std::stod(commands[1]));
			double y(std::stod(commands[2]));
			double w(std::stod(commands[3]));
			double h(std::stod(commands[4]));


			if (0 >= w) {
				throw MyException::IncorrecValuesOAgrumentsException(std::to_string(w));
			} 
			if (0 >= h) {
				throw MyException::IncorrecValuesOAgrumentsException(std::to_string(h));
			}
			if (0 > color || size_t(color) >= colors.size()) {
				throw MyException::IncorrecValuesOAgrumentsException("color " + std::to_string(color));
			}

			return std::make_unique<Rectangle>(color, x, y, w, h);
		}
		catch(const std::invalid_argument&) {
			throw MyException::InvalidCastException();
		}
	}

	std::unique_ptr<Figure> getSquare(std::vector<std::string>& commands) {
		if (4 != commands.size()) {
			throw MyException::IncorrectNumberOfArgumentsException();
		}

		try {
			int color(std::stoi(commands[0]));
			double x(std::stod(commands[1]));
			double y(std::stod(commands[2]));
			double side(std::stod(commands[3]));

			if (0 >= side) {
				throw MyException::IncorrecValuesOAgrumentsException(std::to_string(side));
			}
			if (0 > color || size_t(color) >= colors.size()) {
				throw MyException::IncorrecValuesOAgrumentsException("color " + std::to_string(color));
			}

			return std::make_unique<Square>(color, x, y, side);
		}
		catch(const std::invalid_argument&) {
			throw MyException::InvalidCastException();
		}
	}

	std::unique_ptr<Figure> getTriangle(std::vector<std::string>& commands) {
		if (5 != commands.size()) {
			throw MyException::IncorrectNumberOfArgumentsException();
		}

		try {
			int color(std::stoi(commands[0]));
			double x(std::stod(commands[1]));
			double y(std::stod(commands[2]));
			double a(std::stod(commands[3]));
			double b(std::stod(commands[4]));

			if (0 >= a) {
				throw MyException::IncorrecValuesOAgrumentsException(std::to_string(a));
			}
			if (0 >= b) {
				throw MyException::IncorrecValuesOAgrumentsException(std::to_string(b));
			}
			if (0 > color || size_t(color) >= colors.size()) {
				throw MyException::IncorrecValuesOAgrumentsException("color " + std::to_string(color));
			}

			return std::make_unique<Triangle>(color, x, y, a, b);
		}
		catch(const std::invalid_argument&) {
			throw MyException::InvalidCastException();
		}
	}

	const std::unordered_map<std::string, figureBuilderFunRef> getFigureMap() {
		return std::unordered_map<std::string, figureBuilderFunRef>{
			{"circle", getCircle},
			{"rectangle", getRectangle},
			{"square", getSquare},
			{"triangle", getTriangle},
		};
	}

	void createFigure(InterpreterInterface& interpreter, std::vector<std::string>& commands) {
		static const std::unordered_map<std::string, figureBuilderFunRef>& functions = getFigureMap();

		if (6 > commands.size()) {
			throw MyException::IncorrectNumberOfArgumentsException();
		}
		
		figureBuilderFunRef builder;
		try {
			builder = functions.at(commands[0]);
		}
		catch(const std::out_of_range&) {
			throw MyException::IncorrectCommandNameException(commands[0]);
		}

		commands.erase(commands.begin());

		try {
			int id(std::stoi(commands[0]));
			commands.erase(commands.begin());
			interpreter.addNewFigure(id, builder(commands));
		}
		catch(const std::invalid_argument&) {
			throw MyException::InvalidCastException();
		}
	}

	void setColorForFigure(InterpreterInterface& interpreter, std::vector<std::string>& commands) {
		if (3 != commands.size()) {
			throw MyException::IncorrectNumberOfArgumentsException();
		}

		if ("color" != commands[0]) {
			throw MyException::IncorrectCommandNameException(commands[0]);
		}

		try {
			int id(std::stoi(commands[1]));
			int color(std::stoi(commands[2]));
			if (0 > color || size_t(color) >= colors.size()) {
				throw MyException::IncorrecValuesOAgrumentsException("color " + std::to_string(color));
			}

			interpreter.setColorById(id, color);
		}
		catch(const std::invalid_argument&) {
			throw MyException::InvalidCastException();
		}
	}

	void moveFigure(InterpreterInterface& interpreter, std::vector<std::string>& commands) {
		if (3 != commands.size()) {
			throw MyException::IncorrectNumberOfArgumentsException();
		}

		try {
			int id(std::stoi(commands[0]));
			double x(std::stod(commands[1]));
			double y(std::stod(commands[2]));
			interpreter.moveObject(id, x, y);
		}
		catch(const std::invalid_argument&) {
			throw MyException::InvalidCastException();
		}
	}

	void scaleFigure(InterpreterInterface& interpreter, std::vector<std::string>& commands) {
		if (2 != commands.size()) {
			throw MyException::IncorrectNumberOfArgumentsException();
		}

		try {
			int id(std::stoi(commands[0]));
			double scale(std::stod(commands[1]));

			if (0 >= scale) {
				throw MyException::IncorrecValuesOAgrumentsException(commands[1]);
			}

			interpreter.scaleObject(id, scale);
		}
		catch(const std::invalid_argument&) {
			throw MyException::InvalidCastException();
		}
	}

	void bringFigure(InterpreterInterface& interpreter, std::vector<std::string>& commands) {
		if (3 != commands.size()) {
			throw MyException::IncorrectNumberOfArgumentsException();
		}

		int id(0);
		try {
			id = std::stoi(commands[2]);
		}
		catch(const std::invalid_argument&) {
			throw MyException::InvalidCastException();
		}

		if ("to" == commands[0]) {
			if ("back" == commands[1]) {
				interpreter.bringToBackObject(id);
			} else if ("front" == commands[1]) {
				interpreter.bringToFrontObject(id);
			} else {
				throw MyException::InvalidCastException();
			}
		} else {
			throw MyException::IncorrectCommandNameException(commands[1]);
		}
	}

	void drawPicture(InterpreterInterface& interpreter, std::vector<std::string>& commands) {
		if (7 != commands.size()) {
			throw MyException::IncorrectNumberOfArgumentsException();
		}

		try {
			std::string fileName(commands[0]);
			double sx(std::stod(commands[1]));
			double sy(std::stod(commands[2]));
			double sw(std::stod(commands[3]));
			double sh(std::stod(commands[4]));
			int w(std::stoi(commands[5]));
			int h(std::stoi(commands[6]));

			if (0 >= sw) {
				throw MyException::IncorrecValuesOAgrumentsException(std::to_string(sw));
			}
			if (0 >= sh) {
				throw MyException::IncorrecValuesOAgrumentsException(std::to_string(sh));
			}
			if (0 >= w) {
				throw MyException::IncorrecValuesOAgrumentsException(std::to_string(w));
			}
			if (0 >= h) {
				throw MyException::IncorrecValuesOAgrumentsException(std::to_string(h));
			}

			interpreter.drawPicture(fileName, sx, sy, sw, sh, w, h);
		}
		catch(const std::invalid_argument&) {
			throw MyException::InvalidCastException();
		}
	}

	const std::unordered_map<std::string, funRefHandler> getFunctionFactory() {
		return std::unordered_map<std::string, funRefHandler>{
			{"create", createFigure},
			{"set", setColorForFigure},
			{"move", moveFigure},
			{"scale", scaleFigure},
			{"bring", bringFigure},
			{"draw", drawPicture}
		};
	}

	std::vector<std::string> getCommands(std::string& command) {
		std::vector<std::string> result;
		result.reserve(8);
		static const char delimiter = ' ';
		int start(0);
		int end(0);
		int size(command.size());
		int maxPosition('\r' == command[size - 1] ? size - 1 : size);
		while (end != maxPosition) {
			if (delimiter == command[end]) {
				if (0 != end - start) {
					result.push_back(command.substr(start, end - start));
					++end;
					start = end;
				} else {
					++start;
					++end;
				}
			} else {
				++end;
			}
		}
		if (0 != end - start) {
			result.push_back(command.substr(start, end));
		}
		return result;
	}

	void executeCommand(InterpreterInterface& interpreter, std::string& commandStr, int lineNumber) {
		static const std::unordered_map<std::string, funRefHandler>& functions = getFunctionFactory();
		
		auto commands = getCommands(commandStr);
		try {
			if (3 > commands.size()) {
				throw MyException::IncorrectNumberOfArgumentsException();
			}
			funRefHandler currentFunction;
			try {
				currentFunction = functions.at(commands[0]);
			}
			catch(const std::out_of_range&) {
				throw MyException::IncorrectCommandNameException(commands[0]);
			}
			commands.erase(commands.begin());

			currentFunction(interpreter, commands);
		
		} catch(MyException::InterptreterException& exp) {
			exp.addToMessage("In line \"№" + std::to_string(lineNumber) + "\"");
			throw;
		}
	}

} // namespace

void Parser::parseCommands(const std::string& fileForReadName, InterpreterInterface& interpreter) {
	std::ifstream fileCommands(fileForReadName);
	if (!fileCommands) {
		throw MyException::FileNotFoundException(fileForReadName);
	}

	int lineNumber(0);
	std::string commandLine;
	while (!fileCommands.eof()) {
		++lineNumber;
		std::getline(fileCommands, commandLine);
		int strLength(commandLine.size());
		if (0 == strLength
			|| (1 == strLength && '\r' == commandLine[0])
		) {
			continue;
		}
		executeCommand(interpreter, commandLine, lineNumber);
	}
}	