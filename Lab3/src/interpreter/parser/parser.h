#pragma once

#include <interpreter/interpreter.h>

#include <string>

namespace Parser {
	void parseCommands(const std::string& fileForReadName, InterpreterInterface& interpreter);
}
