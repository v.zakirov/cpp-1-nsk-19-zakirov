#pragma once

#include <interpreter/figure/figure.h>
#include <interpreter/figure/colors.h>

#include <string>
#include <memory>

class InterpreterInterface {
public:
	virtual ~InterpreterInterface() {};
	virtual void addNewFigure(const int id, std::unique_ptr<Figure>&& figure) = 0;
	virtual void setColorById(const int id, const int color) = 0;
	virtual void moveObject(const int id, const double dx, const double dy) = 0;
	virtual void scaleObject(const int id, const double scale) = 0;
	virtual void bringToBackObject(const int id) = 0;
	virtual void bringToFrontObject(const int id) = 0;
	virtual void drawPicture(const std::string& fileName
						, const double sx
						, const double sy
						, const double sw
						, const double sh
						, const int wp
						, const int hp) = 0;
};
