#include "square.h"

Square::Square(const int color, double lx, double ly, double side) 
	: color_(color)
	, lx_(lx)
	, ly_(ly)
	, side_(side)
{}

void Square::setColor(const int color) noexcept {
	color_ = color;
}

void Square::move(const double x, const double y) noexcept {
	lx_ += x;
	ly_ += y;
}

void Square::scale(const double value) noexcept {
	double tSide(side_);
	side_ *= value;
	double dif = (side_ - tSide) / 2;
	lx_ -= dif;
	ly_ -= dif;
}

bool Square::containsDot(const double x, const double y) const {
	return (x >= lx_) && (x <= lx_ + side_) && (y >= ly_) && (y <= ly_ + side_);
}

int Square::getColor() const noexcept {
	return color_;
}
