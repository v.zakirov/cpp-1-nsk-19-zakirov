#pragma once

#include "figure.h"

class Square : public Figure {
public:
	Square(const int color, double lx, double ly, double side);
	void setColor(const int color) noexcept override;
	void move(const double x, const double y) noexcept override;
	void scale(const double value) noexcept override;
	bool containsDot(const double x, const double y) const override;
	int getColor() const noexcept override;

private:
	int color_;
	double lx_;
	double ly_;
	double side_;
};