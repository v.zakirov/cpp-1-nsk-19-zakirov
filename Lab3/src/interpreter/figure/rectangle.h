#pragma once

#include "figure.h"

class Rectangle : public Figure {
public:
	Rectangle(const int color, double lx, double ly, double w, double h);
	void setColor(const int color) noexcept override;
	void move(const double x, const double y) noexcept override;
	void scale(const double value) noexcept override;
	bool containsDot(const double x, const double y) const override;
	int getColor() const noexcept override;

private:
	int color_;
	double lx_;
	double ly_;
	double w_;
	double h_;
};
