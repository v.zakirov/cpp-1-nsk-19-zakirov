#pragma once

#include <array>

const std::array<char, 4> colors = { ' ', '.', 'o', '*' };