#pragma once

#include "figure.h"

class Circle : public Figure {
public:
	Circle(const int color, double xCenter, double yCenter, double radius);
	void setColor(const int color) noexcept override;
	void move(const double x, const double y) noexcept override;
	void scale(const double value) noexcept override;
	bool containsDot(const double x, const double y) const override;
	int getColor() const noexcept override;

private:
	int color_;
	double xCenter_;
	double yCenter_;
	double radius_;
};