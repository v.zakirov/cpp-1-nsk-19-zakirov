#pragma once

class Figure {
public:
	virtual ~Figure() = default;
	virtual void setColor(const int color) noexcept = 0;
	virtual void move(const double x, const double y) noexcept = 0;
	virtual void scale(const double value) noexcept = 0;
	virtual bool containsDot(const double x, const double y) const = 0;
	virtual int getColor() const noexcept = 0;
};