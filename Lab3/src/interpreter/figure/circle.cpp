#include "circle.h"

#include <cmath>

Circle::Circle(const int color, double xCenter, double yCenter, double radius)
	: color_(color)
	, xCenter_(xCenter)
	, yCenter_(yCenter)
	, radius_(radius)
{}

void Circle::setColor(const int color) noexcept {
	color_ = color;
}

void Circle::move(const double x, const double y) noexcept {
	xCenter_ += x;
	yCenter_ += y;
}

void Circle::scale(const double value) noexcept {
	radius_ *= value;
}

bool Circle::containsDot(const double x, const double y) const {
	return sqrt(pow(xCenter_ - x, 2) + pow(yCenter_ - y, 2)) <= radius_ ? true : false;
}

int Circle::getColor() const noexcept {
	return color_;
}
