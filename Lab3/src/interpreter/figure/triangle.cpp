#include "triangle.h"

#include <cmath>

Triangle::Triangle(const int color, double lx, double ly, double firstLeg, double secondLeg)
	: color_(color)
	, lx_(lx)
	, ly_(ly)
	, firstLeg_(firstLeg)
	, secondLeg_(secondLeg)
{}

void Triangle::setColor(const int color) noexcept {
	color_ = color;
}

void Triangle::move(const double x, const double y) noexcept {
	lx_ += x;
	ly_ += y;
}

void Triangle::scale(const double value) noexcept {
	double difX(firstLeg_ / 3);
	double difY(secondLeg_ / 3);
	firstLeg_ *= value;
	secondLeg_ *= value;
	lx_ -= difX * value - difX;
	ly_ -= difY * value - difY;
}

bool Triangle::containsDot(const double x, const double y) const {
	return (x >= lx_) && (y >= ly_) && (y <= (ly_ + secondLeg_ - secondLeg_/firstLeg_ * (x - lx_)));
}

int Triangle::getColor() const noexcept {
	return color_;
}
