#include "rectangle.h"

Rectangle::Rectangle(int color, double lx, double ly, double w, double h)
	: color_(color)
	, lx_(lx)
	, ly_(ly)
	, w_(w)
	, h_(h)
{}

void Rectangle::setColor(const int color) noexcept {
	color_ = color;
}

void Rectangle::move(const double x, const double y) noexcept {
	lx_ += x;
	ly_ += y;
}

void Rectangle::scale(const double value) noexcept {
	double tw(w_);
	double th(h_);
	w_ *= value;
	h_ *= value;
	lx_ -= (w_ - tw) / 2;
	ly_ -= (h_ - th) / 2;
}

bool Rectangle::containsDot(const double x, const double y) const {
	return (x >= lx_) && (x <= lx_ + w_) && (y >= ly_) && (y <= ly_ + h_);
}

int Rectangle::getColor() const noexcept {
	return color_;
}