#pragma once

#include <exception>
#include <string>

namespace MyException {
	
	class InterptreterException : public std::exception {
	public:
		InterptreterException(const std::string& str);
		void addToMessage(const std::string& str);
		const char* what() const noexcept override;

	protected:
		std::string mError_;
	};

	class FileNotFoundException : public InterptreterException {
	public:
		FileNotFoundException(const std::string& str);
	};

	class IncorrectNumberOfArgumentsException : public InterptreterException {
	public:
		IncorrectNumberOfArgumentsException();
	};

	class IncorrectCommandNameException : public InterptreterException {
	public:
		IncorrectCommandNameException(const std::string& str);
	};

	class IncorrecValuesOAgrumentsException : public InterptreterException {
	public:
		IncorrecValuesOAgrumentsException(const std::string& str);
	};

	class InvalidCastException : public InterptreterException {
	public:
		InvalidCastException();
	};

	class AlreadyExistsException : public InterptreterException {
	public:
		AlreadyExistsException(const std::string& str);
	};

	class ElementDoesNotExistException : public InterptreterException {
	public:
		ElementDoesNotExistException(const std::string& str);
	};

} // manespace MyException
