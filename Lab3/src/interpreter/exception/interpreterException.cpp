#include "interpreterException.h"

MyException::InterptreterException::InterptreterException(const std::string& str) : mError_(str) {}

void MyException::InterptreterException::addToMessage(const std::string& str) {
	mError_ = str + "\n" +  mError_;
}

const char* MyException::InterptreterException::what() const noexcept {
	return mError_.c_str();
}


MyException::FileNotFoundException::FileNotFoundException(const std::string& str)
	: InterptreterException("File with this name \"" + str + "\" doesn't exists") {}


MyException::IncorrectNumberOfArgumentsException::IncorrectNumberOfArgumentsException() 
	: InterptreterException("Incorrect number of arguments.") {}


MyException::IncorrectCommandNameException::IncorrectCommandNameException(const std::string& str)
	: InterptreterException("Incorrect comand name \"" + str  + "\"") {}


MyException::IncorrecValuesOAgrumentsException::IncorrecValuesOAgrumentsException(const std::string& str)
	: InterptreterException("Incorrect argument \"" + str + "\"") {}


MyException::InvalidCastException::InvalidCastException()
	: InterptreterException("Invalid cast. ") {}


MyException::AlreadyExistsException::AlreadyExistsException(const std::string& str)
	: InterptreterException("Id \"" + str + "\" already exists") {}


MyException::ElementDoesNotExistException::ElementDoesNotExistException(const std::string& str)
	: InterptreterException("Id \"" + str + "\" does not exist") {}