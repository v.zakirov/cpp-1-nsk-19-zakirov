#pragma once

#include "interpreterInterface.h"

#include <interpreter/figure/figure.h>
#include <interpreter/figure/colors.h>

#include <string>
#include <memory>
#include <unordered_map>
#include <list>

class Interpreter : public InterpreterInterface {
	using listItem = std::shared_ptr<Figure>;
	using listIterator = std::list<listItem>::iterator;

public:
	Interpreter();
	void addNewFigure(const int id, std::unique_ptr<Figure>&& figure) override;
	void setColorById(const int id, const int color) override;
	void moveObject(const int id, const double dx, const double dy) override;
	void scaleObject(const int id, const double scale) override;
	void bringToBackObject(const int id) override;
	void bringToFrontObject(const int id) override;
	void drawPicture(const std::string& fileName
						, const double sx
						, const double sy
						, const double sw
						, const double sh
						, const int wp
						, const int hp) override;

private:
	std::unordered_map<int, listIterator> mapOfIterators_;
	std::list<listItem> listOfFigure;
};
