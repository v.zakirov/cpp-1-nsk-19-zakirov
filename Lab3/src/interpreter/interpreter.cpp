#include "interpreter.h"

#include <interpreter/exception/interpreterException.h>

#include <fstream>
#include <utility>
#include <vector>

Interpreter::Interpreter() {}

void Interpreter::addNewFigure(const int id, std::unique_ptr<Figure>&& figure) {
	auto element = mapOfIterators_.find(id);
	if (element != mapOfIterators_.end()) {
		throw MyException::AlreadyExistsException(std::to_string(id));
	}

	listOfFigure.push_front(std::shared_ptr<Figure>(std::move(figure)));
	mapOfIterators_.insert(std::make_pair(id, listOfFigure.begin()));
}

void Interpreter::setColorById(const int id, const int color) {
	try {
		const auto& value = *(mapOfIterators_[id]);
		value->setColor(color);
	}
	catch(const std::out_of_range&) {
		throw MyException::ElementDoesNotExistException(std::to_string(id));
	}
}

void Interpreter::moveObject(const int id, const double dx, const double dy) {
	try {
		const auto& value = *(mapOfIterators_[id]);
		value->move(dx, dy);
	}
	catch(const std::out_of_range&) {
		throw MyException::ElementDoesNotExistException(std::to_string(id));
	}
}

void Interpreter::scaleObject(const int id, const double scale) {
	try {
		const auto& value = *(mapOfIterators_[id]);
		value->scale(scale);
	}
	catch(const std::out_of_range&) {
		throw MyException::ElementDoesNotExistException(std::to_string(id));
	}
}

void Interpreter::bringToBackObject(const int id) {
	try {
		auto iterator = mapOfIterators_.at(id);
		auto value = std::move(*iterator);
		listOfFigure.erase(iterator);
		listOfFigure.push_back(std::move(value));
		mapOfIterators_[id] = --(listOfFigure.end());
	}
	catch(const std::out_of_range&) {
		throw MyException::ElementDoesNotExistException(std::to_string(id));
	}
}

void Interpreter::bringToFrontObject(const int id) {
	try {
		auto iterator = mapOfIterators_.at(id);
		auto value = std::move(*iterator);
		listOfFigure.erase(iterator);
		listOfFigure.push_front(std::move(value));
		mapOfIterators_[id] = listOfFigure.begin();
	}
	catch(const std::out_of_range&) {
		throw MyException::ElementDoesNotExistException(std::to_string(id));
	}
}

void Interpreter::drawPicture(const std::string& fileName
								, const double sx
								, const double sy
								, const double sw
								, const double sh
								, const int wp
								, const int hp)
{
	std::ofstream out(fileName);
	
	std::pair<int, bool> init{1, false};
	std::vector<std::pair<int, bool>> result;
	result.resize(wp * hp, init);

	double stepX(sw / wp);
	double stepY(sh / hp);
	double startX(sx + stepX / 2);
	double startY(sy + sh - stepY / 2);
	for (const auto& currentFigure : listOfFigure) {
		const auto& figure = *currentFigure;
		const int currentColor = figure.getColor();

		auto y(startY);
		for (int yi = 0; yi < hp; ++yi, y -= stepY) {
			auto x(startX);
			for (int xi = 0; xi < wp; ++xi, x += stepX) {
				auto& it(result[yi * wp + xi]);
				if (!it.second) {
					if (figure.containsDot(x, y)) {
						it.first = currentColor;
						it.second = true;
					}
				}
			}
		}
	}

	for (int yi = 0; yi < hp; ++yi) {
		for (int xi = 0; xi < wp; ++xi) {
			out << colors[result[yi * wp + xi].first];
		}
		out << '\n';
	}
}