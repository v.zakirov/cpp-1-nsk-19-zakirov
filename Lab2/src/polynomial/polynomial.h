#pragma once

#include <vector>
#include <initializer_list>
#include <utility>
#include <ostream>
#include <algorithm>

template<class T>
class Polynomial {
public:
	Polynomial();
	~Polynomial() = default;
	Polynomial(T coef);
	Polynomial(std::initializer_list<T> list);
	Polynomial(const Polynomial&) = default;
	Polynomial& operator=(const Polynomial&) = default;
	Polynomial(Polynomial&& element);
	Polynomial& operator=(Polynomial&& element);

	typename std::vector<T>::iterator begin();
	typename std::vector<T>::iterator end();
	typename std::vector<T>::const_iterator begin() const;
	typename std::vector<T>::const_iterator end() const;

	T& operator[](std::size_t degree);
	const T& operator[](const std::size_t degree) const;
	Polynomial<T>& operator+=(const Polynomial<T>& p);
	Polynomial<T>& operator-=(const Polynomial<T>& p);
	Polynomial<T>& operator*=(const Polynomial<T>& p);
	Polynomial<T>& operator/=(const Polynomial<T>& p);
	Polynomial<T>& operator%=(const Polynomial<T>& p);
	T operator()(T value) const;
	int degree() const;

private:
	mutable std::vector<T> tableOfcoefficients_;
	static const int startSizeOfVector_ = 10;
};

template<class T>
Polynomial<T>::Polynomial() : tableOfcoefficients_(10, 0) {
	tableOfcoefficients_[0] = T(0);
}

template<class T>
Polynomial<T>::Polynomial(T coef) : tableOfcoefficients_(10, 0) {
	tableOfcoefficients_[0] = coef;
}

template<class T>
Polynomial<T>::Polynomial(std::initializer_list<T> list) : tableOfcoefficients_(list) {
}

template<class T>
Polynomial<T>::Polynomial(Polynomial<T>&& element) : tableOfcoefficients_(std::move(element.tableOfcoefficients_)) {
}

template<class T>
Polynomial<T>& Polynomial<T>::operator=(Polynomial<T>&& element) {
	if (this == &element) {
		return *this;
	}
	tableOfcoefficients_ = std::move(element.tableOfcoefficients_);
	return *this;
}

template<class T>
typename std::vector<T>::iterator Polynomial<T>::begin() {
	return tableOfcoefficients_.begin();
}

template<class T>
typename std::vector<T>::iterator Polynomial<T>::end() {
	return tableOfcoefficients_.begin() + degree() + 1;
}

template<class T>
typename std::vector<T>::const_iterator Polynomial<T>::begin() const {
	return tableOfcoefficients_.cbegin();
}

template<class T>
typename std::vector<T>::const_iterator Polynomial<T>::end() const {
	return tableOfcoefficients_.cbegin() + degree() + 1;
}

template<class T>
const T& Polynomial<T>::operator[](const std::size_t degree) const {
	if (tableOfcoefficients_.size() <= degree) {
		tableOfcoefficients_.resize(degree + 1);
	}
	return tableOfcoefficients_[degree];
}

template<class T>
T& Polynomial<T>::operator[](const std::size_t degree) {
	return const_cast<T&>
		(static_cast<const Polynomial<T>&>(*this)
		[degree]);
}

template<class T>
Polynomial<T>& Polynomial<T>::operator+=(const Polynomial<T>& p) {
	int deg = p.degree();
	if (deg > degree()) {
		tableOfcoefficients_.resize(deg + 1);	
	}

	int index = 0;
	for (auto it = p.begin(), end = p.end(); it != end; ++it, ++index) {
		operator[](index) += *it;
	}
	return *this;	
}

template<class T>
Polynomial<T>& Polynomial<T>::operator-=(const Polynomial<T>& p) {
	int deg = p.degree();
	if (deg > degree()) {
		tableOfcoefficients_.resize(deg + 1);	
	}

	int index = 0;
	for (auto it = p.begin(), end = p.end(); it != end; ++it, ++index) {
		operator[](index) -= *it;
	}
	return *this;	
}

template<class T>
Polynomial<T>& Polynomial<T>::operator*=(const Polynomial<T>& p) {
	Polynomial<T> newP(0);
	int pDeg(p.degree());
	for (int firstInd = degree(); firstInd >= 0; --firstInd) {
		for (int secondInd = pDeg; secondInd >= 0 ; --secondInd) {
			newP[firstInd + secondInd] += operator[](firstInd)*p[secondInd];
		}
	}
	*this = newP;
	return *this;
}

template<class T>
Polynomial<T>& Polynomial<T>::operator/=(const Polynomial<T>& p) {
	Polynomial<T> result(0);
	int thisDeg(degree());
	int anotherDeg(p.degree());
	while (-1 != thisDeg && -1 != anotherDeg && thisDeg >= anotherDeg) {
		T coef = operator[](thisDeg) / p[anotherDeg];	
		result[thisDeg - anotherDeg] = coef;
		Polynomial<T> polinom(0);
		polinom[thisDeg - anotherDeg] = coef;
		operator-=(polinom * p);
		thisDeg = degree();
	}
	*this = result;
	return *this;
}	

template<class T>
Polynomial<T>& Polynomial<T>::operator%=(const Polynomial<T>& p) {
	int thisDeg(degree());
	int anotherDeg(p.degree());
	while (-1 != thisDeg && -1 != anotherDeg && thisDeg >= anotherDeg) {
		T coef = operator[](thisDeg) / p[anotherDeg];	
		Polynomial<T> polinom(0);
		polinom[thisDeg - anotherDeg] = coef;
		operator-=(polinom * p);
		thisDeg = degree();
	}
	return *this;
}

template<class T>
T Polynomial<T>::operator()(T value) const {
	T result = 0;
	T paramOfP = 1;
	int currentDegree(degree());
	for (int i = 0; i <= currentDegree; ++i, paramOfP *= value) {
		result += paramOfP * operator[](i);
	}
	return result;
}

template<class T>
int Polynomial<T>::degree() const {
	int index = tableOfcoefficients_.size() - 1;
	for (auto it = tableOfcoefficients_.crbegin(), end = tableOfcoefficients_.crend(); it != end; ++it, --index) {
		if (0 != *it) {
			return index;
		}
	}
	return -1;
}


template<class T>
bool operator==(const Polynomial<T>& p1, const Polynomial<T>& p2) {
	return std::equal(p1.begin(), p1.end(), p2.begin(), p2.end());
}

template<class T>
bool operator!=(const Polynomial<T>& p1, const Polynomial<T>& p2) {
	return !(p1 == p2);
}

template<class T>
Polynomial<T> operator+(const Polynomial<T>& p1, const Polynomial<T>& p2) {
	int deg1(p1.degree());
	int deg2(p2.degree());
	Polynomial<T> newP(deg1 > deg2 ? p1 : p2);
	const Polynomial<T>& pFrom = deg1 > deg2 ? p2 : p1;

	int index = 0;
	for (auto itFrom = pFrom.begin(), end = pFrom.end(); itFrom != end; ++itFrom, ++index) {
		newP[index] += *itFrom;
	}
	return newP;
}

template<class T>
Polynomial<T> operator+(const Polynomial<T>& p, const T value) {
	Polynomial<T> newP = p;
	newP[0] += value;
	return newP;		
}

template<class T>
Polynomial<T> operator+(const T value, const Polynomial<T>& p) {
	return p + value;		
}

template<class T>
Polynomial<T> operator-(const Polynomial<T>& p1, const Polynomial<T>& p2) {
	int deg1(p1.degree());
	int deg2(p2.degree());
	Polynomial<T> newP(deg1 > deg2 ? p1 : p2);
	const Polynomial<T>& pFrom = deg1 > deg2 ? p2 : p1;

	int index = 0;
	for (auto itFrom = pFrom.begin(), end = pFrom.end(); itFrom != end; ++itFrom, ++index) {
		newP[index] -= *itFrom;
	}
	return newP;
}

template<class T>
Polynomial<T> operator-(const Polynomial<T>& p, const T value) {
	Polynomial<T> newP = p;
	newP[0] -= value;
	return newP;		
}

template<class T>
Polynomial<T> operator-(const T value, const Polynomial<T>& p) {
	Polynomial<T> newP = p;
	for (auto it = newP.begin(), end = newP.end(); it != end; ++it) {
		*it *= -1;
	}
	newP[0] += value;
	return newP;		
}

template<class T>
Polynomial<T> operator*(const Polynomial<T>& p1, const Polynomial<T>& p2) {
	Polynomial<T> newP(0);
	int p2Deg(p2.degree());
	for (int firstInd = p1.degree(); firstInd >= 0; --firstInd) {
		for (int secondInd = p2Deg; secondInd >= 0 ; --secondInd) {
			newP[firstInd + secondInd] += p1[firstInd]*p2[secondInd];
		}
	}	
	return newP;
}

template<class T>
Polynomial<T> operator*(const Polynomial<T>& p, const T value) {
	Polynomial<T> newP = p;
	for (auto it = newP.begin(), end = newP.end(); it != end; ++it) {
		*it *= value;
	}
	return newP;
}

template<class T>
Polynomial<T> operator*(const T value, const Polynomial<T>& p) {
	return p * value;
}

template<class T>
std::ostream& operator<<(std::ostream& stream, const Polynomial<T>& item) {
	int deg(item.degree());
	bool alreadyPrint = false;
	if (-1 == deg) {
		stream << T(0);
		return stream;
	}
	if (0 <= deg) {	
		if (0 != item[0]) {
			alreadyPrint = true;
			stream << item[0];
		}
	}
	if (1 <= deg) {
		if (0 != item[1]) {
			if (alreadyPrint) {
				if (0 > item[1]) {
					if (T(-1) == item[1]) {
						stream << " - x";
					} else {
						stream << " - " << -1 * item[1] << "x";
					}
				} else {
					if (T(1) == item[1]) {
						stream << " + " << "x";
					} else {
						stream << " + " << item[1] << "x";
					}
				}
			} else {
				stream << item[1] <<"x";
				alreadyPrint = true;
			}
		}
	}

	for (int i = 2; i <= deg; ++i) {
		if (0 != item[i]) {
			if (alreadyPrint) {
				if (0 > item[i]) {
					if (T(-1) == item[i]) {
						stream << " - " << "x^" << i;
					} else {
						stream << " - " << -1 * item[i] << "x^" << i;
					}
				} else {
					if (T(1) == item[i]) {
						stream << " + " << "x^" << i;
					} else {
						stream << " + " << item[i] << "x^" << i;
					}
				}
			} else {
				alreadyPrint = true;
				if (0 > item[i]) {
					if (T(-1) == item[i]) {
						stream << "-x^" << i;
					} else {
						stream << item[i] << "x^" << i;
					}
				} else {
					if (T(1) == item[i]) {
						stream << "x^" << i;
					} else {
						stream << item[i] << "x^" << i;
					}
				}
			}
		}
	}
	return stream;
}

template<class T>
Polynomial<T> operator/(const Polynomial<T>& p1, const Polynomial<T>& p2) {
	Polynomial<T> pt1 = p1;
	Polynomial<T> pt2 = p2;
	int deg1(pt1.degree());
	int deg2(pt2.degree());
	Polynomial<T> result(0);
	while (-1 != deg1 && -1 != deg2 && deg1 >= deg2) {
		T coef = pt1[deg1] / pt2[deg2];	
		result[deg1 - deg2] = coef;
		Polynomial<T> polinom(0);
		polinom[deg1 - deg2] = coef;
		pt1 -= polinom * pt2;
		deg1 = pt1.degree();
	}
	return result;
}

template<class T>
Polynomial<T> operator/(const Polynomial<T>& p, const T value) {
	Polynomial<T> newP(p);
	for (auto it = newP.begin(), end = newP.end(); it != end; ++it) {
		*it /= value;
	}
	return newP;
}

template<class T>
Polynomial<T> operator/(const T value, const Polynomial<T>& p) {
	return 0 == p.degree() ? Polynomial<T>(value / p[0]) : Polynomial<T>(0);
}

template<class T>
Polynomial<T> operator%(const Polynomial<T>& p1, const Polynomial<T>& p2) {
	Polynomial<T> pt1 = p1;
	Polynomial<T> pt2 = p2;
	int deg1(pt1.degree());
	int deg2(pt2.degree());
	while (-1 != deg1 && -1 != deg2 && deg1 >= deg2) {
		T coef = pt1[deg1] / pt2[deg2];	
		Polynomial<T> polinom(0);
		polinom[deg1 - deg2] = coef;
		pt1 -= polinom * pt2;
		deg1 = pt1.degree();
	}
	return pt1;
}


template<class T>
Polynomial<T> operator%(const Polynomial<T>& p, const T value) {
	return Polynomial<T>(0);
}

template<class T>
Polynomial<T> operator%(const T value, const Polynomial<T>& p) {
	int deg(p.degree());
	if (-1 == deg) {
		return Polynomial<T>(0);
	}
	if (0 == deg) {
		return Polynomial<T>(0);
	}
	return Polynomial<T>(value);
}
