#include "gtest/gtest.h"

#include <polynomial/polynomial.h>

#include <string>
#include <sstream>


TEST(polynomial, operatorsEqualsAndNotEqual) {
	Polynomial<int> p1({1, 2});
	Polynomial<int> p2({1, 2});
	Polynomial<int> p3({2});

	EXPECT_EQ(true, p1 == p2);
	EXPECT_EQ(true, p1 != p3);
}

TEST(polynomial, degree) {
	Polynomial<int> p1({1, 2});
	Polynomial<int> p2(1);
	Polynomial<int> p3(0);

	EXPECT_EQ(1, p1.degree());
	EXPECT_EQ(0, p2.degree());
	EXPECT_EQ(-1, p3.degree());
}

TEST(polynomial, operatorPlus) {
	Polynomial<int> p1({1, 1});
	Polynomial<int> p2({-1, -1});
	Polynomial<int> p3({1, 1});
	Polynomial<int> p4({2, 2});
	Polynomial<int> p5({1, 2});
	Polynomial<int> p6 = p5 + 1;
	p3 += p1;

	EXPECT_EQ(true, p3 == p4);
	EXPECT_EQ(-1, (p1 + p2).degree());
	EXPECT_EQ(true, p6 == p4);
}

TEST(polynomial, opedratorMinus) {
	Polynomial<int> p1({1, 1});
	Polynomial<int> p2({-1, -1});
	Polynomial<int> p3({1, 1});
	Polynomial<int> p4({2, 2});
	Polynomial<int> p5({3, 2});
	Polynomial<int> p6 = p5 - 1;
	Polynomial<int> p7({-2, -2});
	p2 -= p1;

	EXPECT_EQ(-1, (p1 - p3).degree());
	EXPECT_EQ(true, p6 == p4);
	EXPECT_EQ(true, p2 == p7);
}

TEST(polynomial, opedratorMultiply) {
	Polynomial<int> p1({1, 1});
	Polynomial<int> p2 = p1 * p1;
	Polynomial<int> p3({1, 2, 1});
	Polynomial<int> p4 = p1 * 3;
	Polynomial<int> p5 = 3 * p1;
	Polynomial<int> p6({-1});
	Polynomial<int> p7({-2, -2});
	Polynomial<int> p8({2, 2});
	p7 *= p6;

	EXPECT_EQ(true, p2 == p3);
	EXPECT_EQ(true, p4 == p5);
	EXPECT_EQ(true, p7 == p8);
}

TEST(polynomial, operatorDivision) {
	Polynomial<int> p1({0, 1, 0, -1, 0, 2, 1});
	Polynomial<int> p2({2, -4, 0, 0, 1});
	Polynomial<int> p3({0, 2, 1});
	Polynomial<int> p4({0, -3, 6, 3});
	Polynomial<int> p5({2, 2, 2, 2});	
	Polynomial<int> p9({2, 2, 2, 2});
	Polynomial<int> p6({1, 1, 1, 1});
	p5 /= 2;
	auto p7 = 10 / p6;
	auto p8 = p9 / 2;

	EXPECT_EQ(true, (p1 / p2) == p3);
	EXPECT_EQ(true, (p1 % p2) == p4);
	EXPECT_EQ(true, p5 == p6);
	EXPECT_EQ(true, p7 == Polynomial<int>(0));
	EXPECT_EQ(true, p8 == p6);
}

TEST(polynomial, operatorRemains) {
	Polynomial<int> p1({2, 2, 2, 2});
	Polynomial<int> p2({2});
	auto p3 = p1 % p2;
	Polynomial<int> p4({1, 1, -1, 1});
	Polynomial<int> p5({-1, 0, 1});
	Polynomial<int> p6({0, 2});
	p4 %= p5;

	EXPECT_EQ(true, Polynomial<int>() == p1 % 5);
	EXPECT_EQ(true, Polynomial<int>(5) == 5 % p1);
	EXPECT_EQ(true, Polynomial<int>() == 5 % p2);
	EXPECT_EQ(true, Polynomial<int>(0) == p3);
	EXPECT_EQ(true, p6 == p4);
}

TEST(polynomial, accessOpedrator) {
	Polynomial<int> p1({1});
	Polynomial<int> p2({1, 1});
	Polynomial<int> p3({0, 0, 3});
	Polynomial<int> p4({1, 1, 1});
	p3[2] = 0;
	p1[10] = 1;
	p2[2] = 1;
	
	EXPECT_EQ(-1, p3.degree());
	EXPECT_EQ(10, p1.degree());
	EXPECT_EQ(true, p2 == p4);
}

TEST(polynomial, opedratorValue) {
	Polynomial<int> p1({1});
	Polynomial<int> p2({1, 1});
	Polynomial<int> p3({0, 0, 0, 1});
	Polynomial<int> p4({1, 0, 0, 1});

	EXPECT_EQ(1, p1(3));
	EXPECT_EQ(3, p2(2));
	EXPECT_EQ(8, p3(2));
	EXPECT_EQ(0, p4(-1));
}

TEST(polynomial, operatorOut) {
	Polynomial<int> p1;
	Polynomial<int> p2({1});
	Polynomial<int> p3({1, 1});
	Polynomial<int> p4({1, 1, 2});
	Polynomial<int> p5({0, 3});
	Polynomial<int> p6({0, 0, 0, 1});
	Polynomial<int> p7({6, 5, 4, 3, 5});

	std::stringstream str1;
	str1 << p1;

	std::stringstream str2;
	str2 << p2;

	std::stringstream str3;
	str3 << p3;

	std::stringstream str4;
	str4 << p4;

	std::stringstream str5;
	str5 << p5;

	std::stringstream str6;
	str6 << p6;

	std::stringstream str7;
	str7 << p7;

	EXPECT_EQ(std::string("0"), str1.str());
	EXPECT_EQ(std::string("1"), str2.str());
	EXPECT_EQ(std::string("1 + x"), str3.str());
	EXPECT_EQ(std::string("1 + x + 2x^2"), str4.str());
	EXPECT_EQ(std::string("3x"), str5.str());
	EXPECT_EQ(std::string("x^3"), str6.str());
	EXPECT_EQ(std::string("6 + 5x + 4x^2 + 3x^3 + 5x^4"), str7.str());
}
