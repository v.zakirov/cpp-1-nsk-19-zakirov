# Lab work number two


```

# clone git repo
git clone https://gitlab.com/v.zakirov/cpp-1-nsk-19-zakirov.git
cd Lab2

# create temp directories for building
mkdir build

# generate, build
cd build
cmake ../
make

# run
./polynomialTests
```